# TODO.md Template 

This is a simple TODO.md template. 

Herein lies the description.

## Todo 

- [ ] This is an example task -- Estimate | Tag | Owner | Date time 
- [ ] This is another example task ~3d #bug @Frank 14-05-2022
- [ ] A task with dependencies or acceptance criteria 22-07-2022  
  - [ ] Subtask or conditions

## Doing 
- [ ] Design a logo @Jane 
- [ ] Send event invites ~2d @Frank

## Done ✓
- [x] Create my first TODO.md
